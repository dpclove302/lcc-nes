
module lcc {

export module Utils {

/**
 * ArrayBuffer 转 binary string
 */
export function ab2bs(ab:ArrayBuffer){
	return new Promise<string>((resolve)=>{
		var b = new Blob([ab]);
		var r = new FileReader();
		r.readAsBinaryString(b);
		r.onload = ()=>{
			resolve(r.result as string);
		}
	});
}

}

}
