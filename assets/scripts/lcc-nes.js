var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var lcc;
(function (lcc) {
    var RingBuffer = (function () {
        function RingBuffer(capacity, evictedCb) {
            this._elements = null;
            this._first = 0;
            this._last = 0;
            this._size = 0;
            this._evictedCb = null;
            this._elements = new Array(capacity || 50);
            this._first = 0;
            this._last = 0;
            this._size = 0;
            this._evictedCb = evictedCb;
        }
        RingBuffer.prototype.capacity = function () {
            return this._elements.length;
        };
        RingBuffer.prototype.size = function () {
            return this._size;
        };
        RingBuffer.prototype.isEmpty = function () {
            return this.size() === 0;
        };
        RingBuffer.prototype.isFull = function () {
            return this.size() === this.capacity();
        };
        RingBuffer.prototype.peek = function () {
            if (this.isEmpty())
                throw new Error('RingBuffer is empty');
            return this._elements[this._first];
        };
        RingBuffer.prototype.peekN = function (count) {
            if (count > this._size)
                throw new Error('Not enough elements in RingBuffer');
            var end = Math.min(this._first + count, this.capacity());
            var firstHalf = this._elements.slice(this._first, end);
            if (end < this.capacity()) {
                return firstHalf;
            }
            var secondHalf = this._elements.slice(0, count - firstHalf.length);
            return firstHalf.concat(secondHalf);
        };
        RingBuffer.prototype.deq = function () {
            var element = this.peek();
            this._size--;
            this._first = (this._first + 1) % this.capacity();
            return element;
        };
        RingBuffer.prototype.deqN = function (count) {
            var elements = this.peekN(count);
            this._size -= count;
            this._first = (this._first + count) % this.capacity();
            return elements;
        };
        RingBuffer.prototype.enq = function (element) {
            this._last = (this._first + this.size()) % this.capacity();
            var full = this.isFull();
            if (full && this._evictedCb) {
                this._evictedCb(this._elements[this._last]);
            }
            this._elements[this._last] = element;
            if (full) {
                this._first = (this._first + 1) % this.capacity();
            }
            else {
                this._size++;
            }
            return this.size();
        };
        return RingBuffer;
    }());
    lcc.RingBuffer = RingBuffer;
})(lcc || (lcc = {}));
var lcc;
(function (lcc) {
    var Utils;
    (function (Utils) {
        function ab2bs(ab) {
            return new Promise(function (resolve) {
                var b = new Blob([ab]);
                var r = new FileReader();
                r.readAsBinaryString(b);
                r.onload = function () {
                    resolve(r.result);
                };
            });
        }
        Utils.ab2bs = ab2bs;
    })(Utils = lcc.Utils || (lcc.Utils = {}));
})(lcc || (lcc = {}));
var lcc;
(function (lcc) {
    var nes;
    (function (nes) {
        var BUFFERSIZE = 8192;
        var AudioPlayer = (function () {
            function AudioPlayer() {
                this._buffer = null;
                this._audioCtx = null;
                this._scriptNode = null;
                this._buffer = new lcc.RingBuffer(BUFFERSIZE * 2);
            }
            AudioPlayer.prototype.getSampleRate = function () {
                if (!AudioContext) {
                    return 44100;
                }
                var myCtx = new AudioContext();
                var sampleRate = myCtx.sampleRate;
                myCtx.close();
                return sampleRate;
            };
            AudioPlayer.prototype.start = function () {
                if (!this._scriptNode) {
                    if (!AudioContext) {
                        return;
                    }
                    this._audioCtx = new AudioContext();
                    this._scriptNode = this._audioCtx.createScriptProcessor(1024, 0, 2);
                    this._scriptNode.onaudioprocess = this.onaudioprocess.bind(this);
                    this._scriptNode.connect(this._audioCtx.destination);
                }
            };
            AudioPlayer.prototype.stop = function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (this._scriptNode) {
                                    this._scriptNode.disconnect(this._audioCtx.destination);
                                    this._scriptNode.onaudioprocess = null;
                                    this._scriptNode = null;
                                }
                                if (!this._audioCtx) return [3, 2];
                                return [4, this._audioCtx.close()];
                            case 1:
                                _a.sent();
                                this._audioCtx = null;
                                _a.label = 2;
                            case 2: return [2];
                        }
                    });
                });
            };
            AudioPlayer.prototype.writeSample = function (left, right) {
                if (this._buffer.size() / 2 >= BUFFERSIZE) {
                    this._buffer.deqN(BUFFERSIZE / 2);
                }
                this._buffer.enq(left);
                this._buffer.enq(right);
            };
            ;
            AudioPlayer.prototype.onaudioprocess = function (e) {
                var left = e.outputBuffer.getChannelData(0);
                var right = e.outputBuffer.getChannelData(1);
                var size = left.length;
                try {
                    var samples = this._buffer.deqN(size * 2);
                }
                catch (e) {
                    var bufferSize = this._buffer.size() / 2;
                    if (bufferSize > 0) {
                    }
                    for (var j = 0; j < size; j++) {
                        left[j] = 0;
                        right[j] = 0;
                    }
                    return;
                }
                for (var i = 0; i < size; i++) {
                    left[i] = samples[i * 2];
                    right[i] = samples[i * 2 + 1];
                }
            };
            ;
            return AudioPlayer;
        }());
        nes.AudioPlayer = AudioPlayer;
    })(nes = lcc.nes || (lcc.nes = {}));
})(lcc || (lcc = {}));
var lcc;
(function (lcc) {
    var nes;
    (function (nes) {
        var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
        var FRAMESIZE = cc.size(256, 240);
        var gfx = cc.gfx;
        var Emulator = (function (_super) {
            __extends(Emulator, _super);
            function Emulator() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this._rom = null;
                _this._prepare = false;
                _this._nes = null;
                _this._texture = null;
                _this._framebuff = null;
                _this._frameu8 = null;
                _this._frameu32 = null;
                _this._audio = null;
                return _this;
            }
            Object.defineProperty(Emulator.prototype, "rom", {
                get: function () {
                    return this._rom;
                },
                set: function (value) {
                    if (this._rom != value) {
                        this._rom = value;
                        this.setRomData(value);
                    }
                },
                enumerable: true,
                configurable: true
            });
            Emulator.prototype.onLoad = function () {
                this._audio = new nes.AudioPlayer();
                this._nes = new jsnes.NES({
                    onFrame: this.setFrameBuffer.bind(this),
                    onStatusUpdate: cc.log,
                    sampleRate: this._audio.getSampleRate(),
                    onAudioSample: this._audio.writeSample.bind(this._audio),
                });
                this._texture = new cc.Texture2D();
                this._framebuff = new ArrayBuffer(FRAMESIZE.width * FRAMESIZE.height * 4);
                this._frameu8 = new Uint8Array(this._framebuff);
                this._frameu32 = new Uint32Array(this._framebuff);
            };
            Emulator.prototype.setFrameBuffer = function (buffer) {
                var i = 0;
                for (var y = 0; y < FRAMESIZE.height; ++y) {
                    for (var x = 0; x < FRAMESIZE.width; ++x) {
                        i = y * 256 + x;
                        this._frameu32[i] = 0xff000000 | buffer[i];
                    }
                }
                this._texture.initWithData(this._frameu8, gfx.TEXTURE_FMT_RGBA8, FRAMESIZE.width, FRAMESIZE.height);
            };
            Emulator.prototype.getNES = function () {
                return this._nes;
            };
            Emulator.prototype.getTexture = function () {
                return this._texture;
            };
            Emulator.prototype.setRomData = function (data) {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, _b;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                if (!data) return [3, 2];
                                _b = (_a = this._nes).loadROM;
                                return [4, lcc.Utils.ab2bs(data._buffer)];
                            case 1:
                                _b.apply(_a, [_c.sent()]);
                                this._audio.start();
                                this._prepare = true;
                                return [3, 3];
                            case 2:
                                this._audio.stop();
                                this._prepare = false;
                                _c.label = 3;
                            case 3: return [2];
                        }
                    });
                });
            };
            Emulator.prototype.update = function (dt) {
                if (this._prepare) {
                    this._nes.frame();
                }
            };
            Emulator.prototype.start = function () {
                if (this.rom) {
                    this.setRomData(this.rom);
                }
            };
            Emulator.prototype.saveArchive = function () {
                return this._nes.saveArchive();
            };
            Emulator.prototype.loadArchive = function (json) {
                this._nes.loadArchive(json);
            };
            __decorate([
                property(cc.BufferAsset)
            ], Emulator.prototype, "_rom", void 0);
            __decorate([
                property({
                    type: cc.BufferAsset,
                    tooltip: "ROM 数据"
                })
            ], Emulator.prototype, "rom", null);
            Emulator = __decorate([
                ccclass("lcc.nes.Emulator"),
                menu("i18n:lcc-nes.menu_component/Emulator")
            ], Emulator);
            return Emulator;
        }(cc.Component));
        nes.Emulator = Emulator;
    })(nes = lcc.nes || (lcc.nes = {}));
})(lcc || (lcc = {}));
var lcc;
(function (lcc) {
    var nes;
    (function (nes) {
        var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, requireComponent = _a.requireComponent, menu = _a.menu;
        var Player;
        (function (Player) {
            Player[Player["PLAYER_1"] = 1] = "PLAYER_1";
            Player[Player["PLAYER_2"] = 2] = "PLAYER_2";
        })(Player = nes.Player || (nes.Player = {}));
        var Button;
        (function (Button) {
            Button[Button["A"] = 0] = "A";
            Button[Button["B"] = 1] = "B";
            Button[Button["SELECT"] = 2] = "SELECT";
            Button[Button["START"] = 3] = "START";
            Button[Button["UP"] = 4] = "UP";
            Button[Button["DOWN"] = 5] = "DOWN";
            Button[Button["LEFT"] = 6] = "LEFT";
            Button[Button["RIGHT"] = 7] = "RIGHT";
        })(Button = nes.Button || (nes.Button = {}));
        var ButtonMap = (function () {
            function ButtonMap() {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                this.button = Button.A;
                this.key = cc.macro.KEY.w;
                this.button = args[0] || Button.A;
                this.key = args[1] || cc.macro.KEY.w;
            }
            __decorate([
                property({
                    type: cc.Enum(Button),
                    tooltip: "游戏按钮",
                    readonly: true,
                })
            ], ButtonMap.prototype, "button", void 0);
            __decorate([
                property({
                    type: cc.Enum(cc.macro.KEY),
                    tooltip: "键盘按键"
                })
            ], ButtonMap.prototype, "key", void 0);
            ButtonMap = __decorate([
                ccclass("lcc.nes.ButtonMap")
            ], ButtonMap);
            return ButtonMap;
        }());
        nes.ButtonMap = ButtonMap;
        var Controller = (function (_super) {
            __extends(Controller, _super);
            function Controller() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.player = Player.PLAYER_1;
                _this._buttons = [
                    new ButtonMap(Button.A, cc.macro.KEY.j),
                    new ButtonMap(Button.B, cc.macro.KEY.k),
                    new ButtonMap(Button.SELECT, cc.macro.KEY.f),
                    new ButtonMap(Button.START, cc.macro.KEY.h),
                    new ButtonMap(Button.UP, cc.macro.KEY.w),
                    new ButtonMap(Button.DOWN, cc.macro.KEY.s),
                    new ButtonMap(Button.LEFT, cc.macro.KEY.a),
                    new ButtonMap(Button.RIGHT, cc.macro.KEY.d)
                ];
                _this._nes = null;
                _this._keymap = null;
                return _this;
            }
            Object.defineProperty(Controller.prototype, "buttons", {
                get: function () {
                    return this._buttons;
                },
                set: function (value) {
                    if (this._buttons != value) {
                        this._buttons = value;
                        this.updateButtonMap();
                    }
                },
                enumerable: true,
                configurable: true
            });
            Controller.prototype.onLoad = function () {
                this._nes = this.getComponent(nes.Emulator).getNES();
                this.updateButtonMap();
                cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
                cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
                this.node.on("nes_button_event", this.onNodeButtonEvent, this);
            };
            Controller.prototype.onDestroy = function () {
                cc.systemEvent.targetOff(this);
                this.node.targetOff(this);
            };
            Controller.prototype.onNodeButtonEvent = function (player, button, down) {
                if (player == this.player) {
                    this.onButtonEvent(button, down);
                }
            };
            Controller.prototype.onButtonEvent = function (button, down) {
                if (down) {
                    this._nes.buttonDown(this.player, button);
                }
                else {
                    this._nes.buttonUp(this.player, button);
                }
            };
            Controller.prototype.onKeyDown = function (event) {
                var button = this._keymap[event.keyCode];
                if (button != null) {
                    this.onButtonEvent(button, true);
                }
            };
            Controller.prototype.onKeyUp = function (event) {
                var button = this._keymap[event.keyCode];
                if (button != null) {
                    this.onButtonEvent(button, false);
                }
            };
            Controller.prototype.updateButtonMap = function () {
                this._keymap = {};
                for (var _i = 0, _a = this._buttons; _i < _a.length; _i++) {
                    var b = _a[_i];
                    this._keymap[b.key] = b.button;
                }
            };
            __decorate([
                property({
                    type: cc.Enum(Player),
                    tooltip: "玩家序号"
                })
            ], Controller.prototype, "player", void 0);
            __decorate([
                property([ButtonMap])
            ], Controller.prototype, "_buttons", void 0);
            __decorate([
                property({
                    type: [ButtonMap],
                    tooltip: "按键映射表"
                })
            ], Controller.prototype, "buttons", null);
            Controller = __decorate([
                ccclass("lcc.nes.Controller"),
                requireComponent(nes.Emulator),
                menu("i18n:lcc-nes.menu_component/Controller")
            ], Controller);
            return Controller;
        }(cc.Component));
        nes.Controller = Controller;
    })(nes = lcc.nes || (lcc.nes = {}));
})(lcc || (lcc = {}));
var lcc;
(function (lcc) {
    var nes;
    (function (nes) {
        var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, requireComponent = _a.requireComponent, menu = _a.menu;
        var DisplaySprites = (function (_super) {
            __extends(DisplaySprites, _super);
            function DisplaySprites() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this._sprites = [];
                _this._emulator = null;
                return _this;
            }
            Object.defineProperty(DisplaySprites.prototype, "sprites", {
                get: function () {
                    return this._sprites;
                },
                set: function (value) {
                    if (this._sprites != value) {
                        this._sprites = value;
                        this.setDisplaySprites(value);
                    }
                },
                enumerable: true,
                configurable: true
            });
            DisplaySprites.prototype.onLoad = function () {
                this._emulator = this.getComponent(nes.Emulator);
                this.setDisplaySprites(this.sprites);
            };
            DisplaySprites.prototype.setDisplaySprites = function (sprites) {
                if (sprites && sprites.length > 0) {
                    var spriteFrame = new cc.SpriteFrame(this._emulator.getTexture());
                    for (var _i = 0, sprites_1 = sprites; _i < sprites_1.length; _i++) {
                        var sp_1 = sprites_1[_i];
                        sp_1.spriteFrame = spriteFrame;
                    }
                }
            };
            __decorate([
                property([cc.Sprite])
            ], DisplaySprites.prototype, "_sprites", void 0);
            __decorate([
                property({
                    type: [cc.Sprite],
                    tooltip: "展示的Sprite数组"
                })
            ], DisplaySprites.prototype, "sprites", null);
            DisplaySprites = __decorate([
                ccclass("lcc.nes.DisplaySprites"),
                requireComponent(nes.Emulator),
                menu("i18n:lcc-nes.menu_component/DisplaySprites")
            ], DisplaySprites);
            return DisplaySprites;
        }(cc.Component));
        nes.DisplaySprites = DisplaySprites;
    })(nes = lcc.nes || (lcc.nes = {}));
})(lcc || (lcc = {}));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZyYW1ld29yay9zcmMvY29tbW9uL1JpbmdCdWZmZXIudHMiLCJmcmFtZXdvcmsvc3JjL2NvbW1vbi9VdGlscy50cyIsImZyYW1ld29yay9zcmMvbmVzL0F1ZGlvUGxheWVyLnRzIiwiZnJhbWV3b3JrL3NyYy9uZXMvRW11bGF0b3IudHMiLCJmcmFtZXdvcmsvc3JjL25lcy9Db250cm9sbGVyLnRzIiwiZnJhbWV3b3JrL3NyYy9uZXMvRGlzcGxheVNwcml0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLElBQU8sR0FBRyxDQWdLVDtBQWhLRCxXQUFPLEdBQUc7SUFLVjtRQW1CQyxvQkFBWSxRQUFlLEVBQUUsU0FBbUI7WUFqQnhDLGNBQVMsR0FBUyxJQUFJLENBQUM7WUFDdkIsV0FBTSxHQUFVLENBQUMsQ0FBQztZQUNsQixVQUFLLEdBQVUsQ0FBQyxDQUFDO1lBQ2pCLFVBQUssR0FBVSxDQUFDLENBQUM7WUFDakIsZUFBVSxHQUFZLElBQUksQ0FBQztZQWNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksS0FBSyxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUNoQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDN0IsQ0FBQztRQVFNLDZCQUFRLEdBQWY7WUFDQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1FBQzlCLENBQUM7UUFRTSx5QkFBSSxHQUFYO1lBQ0MsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ25CLENBQUM7UUFRTSw0QkFBTyxHQUFkO1lBQ0MsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFCLENBQUM7UUFRTSwyQkFBTSxHQUFiO1lBQ0MsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hDLENBQUM7UUFTTSx5QkFBSSxHQUFYO1lBQ0MsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUMzRCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BDLENBQUM7UUFTTSwwQkFBSyxHQUFaLFVBQWEsS0FBWTtZQUN4QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSztnQkFBRSxNQUFNLElBQUksS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7WUFFN0UsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUN6RCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZELElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBRTtnQkFDeEIsT0FBTyxTQUFTLENBQUM7YUFDbkI7WUFDRCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNuRSxPQUFPLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDckMsQ0FBQztRQVNNLHdCQUFHLEdBQVY7WUFDQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFMUIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2IsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBRWxELE9BQU8sT0FBTyxDQUFDO1FBQ2hCLENBQUM7UUFTTSx5QkFBSSxHQUFYLFVBQVksS0FBWTtZQUN2QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRWpDLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUV0RCxPQUFPLFFBQVEsQ0FBQztRQUNqQixDQUFDO1FBU00sd0JBQUcsR0FBVixVQUFXLE9BQVc7WUFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQzNELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtZQUN4QixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7YUFDN0M7WUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxPQUFPLENBQUM7WUFFckMsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ25EO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNkO1lBRUQsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEIsQ0FBQztRQUNGLGlCQUFDO0lBQUQsQ0F6SkEsQUF5SkMsSUFBQTtJQXpKWSxjQUFVLGFBeUp0QixDQUFBO0FBRUQsQ0FBQyxFQWhLTSxHQUFHLEtBQUgsR0FBRyxRQWdLVDtBQ2hLRCxJQUFPLEdBQUcsQ0FvQlQ7QUFwQkQsV0FBTyxHQUFHO0lBRVYsSUFBYyxLQUFLLENBZ0JsQjtJQWhCRCxXQUFjLEtBQUs7UUFLbkIsU0FBZ0IsS0FBSyxDQUFDLEVBQWM7WUFDbkMsT0FBTyxJQUFJLE9BQU8sQ0FBUyxVQUFDLE9BQU87Z0JBQ2xDLElBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztnQkFDekIsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixDQUFDLENBQUMsTUFBTSxHQUFHO29CQUNWLE9BQU8sQ0FBQyxDQUFDLENBQUMsTUFBZ0IsQ0FBQyxDQUFDO2dCQUM3QixDQUFDLENBQUE7WUFDRixDQUFDLENBQUMsQ0FBQztRQUNKLENBQUM7UUFUZSxXQUFLLFFBU3BCLENBQUE7SUFFRCxDQUFDLEVBaEJhLEtBQUssR0FBTCxTQUFLLEtBQUwsU0FBSyxRQWdCbEI7QUFFRCxDQUFDLEVBcEJNLEdBQUcsS0FBSCxHQUFHLFFBb0JUO0FDcEJELElBQU8sR0FBRyxDQXVHVDtBQXZHRCxXQUFPLEdBQUc7SUFFVixJQUFjLEdBQUcsQ0FtR2hCO0lBbkdELFdBQWMsR0FBRztRQUVqQixJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFLeEI7WUFVQztnQkFMUSxZQUFPLEdBQWMsSUFBSSxDQUFDO2dCQUUxQixjQUFTLEdBQWdCLElBQUksQ0FBQztnQkFDOUIsZ0JBQVcsR0FBdUIsSUFBSSxDQUFDO2dCQUc5QyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksSUFBQSxVQUFVLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQy9DLENBQUM7WUFFRCxtQ0FBYSxHQUFiO2dCQUNDLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ2pCLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2dCQUNELElBQUksS0FBSyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7Z0JBQy9CLElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7Z0JBQ2xDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDZCxPQUFPLFVBQVUsQ0FBQztZQUNuQixDQUFDO1lBRUQsMkJBQUssR0FBTDtnQkFDQyxJQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBQztvQkFFcEIsSUFBSSxDQUFDLFlBQVksRUFBRTt3QkFDbEIsT0FBTztxQkFDUDtvQkFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNwRSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDakUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztpQkFDckQ7WUFDRixDQUFDO1lBRUssMEJBQUksR0FBVjs7Ozs7Z0NBQ0MsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29DQUNuQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29DQUN4RCxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7b0NBQ3ZDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2lDQUMxQjtxQ0FDRyxJQUFJLENBQUMsU0FBUyxFQUFkLGNBQWM7Z0NBQ2YsV0FBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxFQUFBOztnQ0FBNUIsU0FBNEIsQ0FBQztnQ0FDN0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7Ozs7OzthQUV6QjtZQUVELGlDQUFXLEdBQVgsVUFBWSxJQUFJLEVBQUUsS0FBSztnQkFDdEIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsSUFBSSxVQUFVLEVBQUU7b0JBRXhDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDcEM7Z0JBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pCLENBQUM7WUFBQSxDQUFDO1lBRUYsb0NBQWMsR0FBZCxVQUFlLENBQUM7Z0JBQ2YsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzVDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQU92QixJQUFJO29CQUNELElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDNUM7Z0JBQUMsT0FBTyxDQUFDLEVBQUU7b0JBS1gsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7b0JBQ3pDLElBQUksVUFBVSxHQUFHLENBQUMsRUFBRTtxQkFFbkI7b0JBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRTt3QkFDOUIsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDWixLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUNiO29CQUNELE9BQU87aUJBQ1A7Z0JBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDNUIsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDaEM7WUFDRixDQUFDO1lBQUEsQ0FBQztZQUNILGtCQUFDO1FBQUQsQ0ExRkEsQUEwRkMsSUFBQTtRQTFGWSxlQUFXLGNBMEZ2QixDQUFBO0lBRUQsQ0FBQyxFQW5HYSxHQUFHLEdBQUgsT0FBRyxLQUFILE9BQUcsUUFtR2hCO0FBRUQsQ0FBQyxFQXZHTSxHQUFHLEtBQUgsR0FBRyxRQXVHVDtBQ3ZHRCxJQUFPLEdBQUcsQ0F3S1Q7QUF4S0QsV0FBTyxHQUFHO0lBRVYsSUFBYyxHQUFHLENBb0toQjtJQXBLRCxXQUFjLEdBQUc7UUFFWCxJQUFBLGtCQUEwQyxFQUF6QyxvQkFBTyxFQUFFLHNCQUFRLEVBQUUsY0FBc0IsQ0FBQztRQVVqRCxJQUFNLFNBQVMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBQyxHQUFHLENBQUMsQ0FBQztRQUduQyxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDO1FBSWpCO1lBQThCLDRCQUFZO1lBQTFDO2dCQUFBLHFFQStJQztnQkE1SUcsVUFBSSxHQUFtQixJQUFJLENBQUM7Z0JBa0JwQixjQUFRLEdBQVcsS0FBSyxDQUFDO2dCQUt6QixVQUFJLEdBQU8sSUFBSSxDQUFDO2dCQUtoQixjQUFRLEdBQWdCLElBQUksQ0FBQztnQkFLN0IsZ0JBQVUsR0FBZSxJQUFJLENBQUM7Z0JBSzlCLGNBQVEsR0FBYyxJQUFJLENBQUM7Z0JBSzlCLGVBQVMsR0FBZSxJQUFJLENBQUM7Z0JBSzdCLFlBQU0sR0FBZSxJQUFJLENBQUM7O1lBNEZuQyxDQUFDO1lBdklBLHNCQUFJLHlCQUFHO3FCQUFQO29CQUNDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDbEIsQ0FBQztxQkFDRCxVQUFRLEtBQW9CO29CQUMzQixJQUFHLElBQUksQ0FBQyxJQUFJLElBQUksS0FBSyxFQUFDO3dCQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQzt3QkFDbEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDdkI7Z0JBQ0MsQ0FBQzs7O2VBTkg7WUE2Q0UseUJBQU0sR0FBTjtnQkFDRixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksSUFBQSxXQUFXLEVBQUUsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUM7b0JBQ3pCLE9BQU8sRUFBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ3hDLGNBQWMsRUFBRyxFQUFFLENBQUMsR0FBRztvQkFDdkIsVUFBVSxFQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFO29CQUN4QyxhQUFhLEVBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7aUJBQ3pELENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUNuQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDMUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2hELENBQUM7WUFLTyxpQ0FBYyxHQUF0QixVQUF1QixNQUFrQjtnQkFDckMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNWLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO29CQUN2QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsRUFBRTt3QkFDdEMsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDO3dCQUVoQixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLFVBQVUsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQzlDO2lCQUNKO2dCQUVELElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLGlCQUFpQixFQUFFLFNBQVMsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3hHLENBQUM7WUFLTSx5QkFBTSxHQUFiO2dCQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztZQUNyQixDQUFDO1lBS00sNkJBQVUsR0FBakI7Z0JBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3pCLENBQUM7WUFLWSw2QkFBVSxHQUF2QixVQUF3QixJQUFtQjs7Ozs7O3FDQUMxQyxJQUFJLEVBQUosY0FBSTtnQ0FFTixLQUFBLENBQUEsS0FBQSxJQUFJLENBQUMsSUFBSSxDQUFBLENBQUMsT0FBTyxDQUFBO2dDQUFDLFdBQU0sSUFBQSxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBQTs7Z0NBQWpELGNBQWtCLFNBQStCLEVBQUMsQ0FBQztnQ0FDbkQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQ0FDcEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7OztnQ0FFckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDbkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7Ozs7OzthQUVwQjtZQUVELHlCQUFNLEdBQU4sVUFBTyxFQUFFO2dCQUNMLElBQUcsSUFBSSxDQUFDLFFBQVEsRUFBQztvQkFDYixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUNyQjtZQUNMLENBQUM7WUFFRCx3QkFBSyxHQUFMO2dCQUNJLElBQUcsSUFBSSxDQUFDLEdBQUcsRUFBQztvQkFDUixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDN0I7WUFDUixDQUFDO1lBTU0sOEJBQVcsR0FBbEI7Z0JBQ0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ2hDLENBQUM7WUFNTSw4QkFBVyxHQUFsQixVQUFtQixJQUFJO2dCQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3QixDQUFDO1lBeklFO2dCQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO2tEQUNHO1lBSy9CO2dCQUpDLFFBQVEsQ0FBQztvQkFDVCxJQUFJLEVBQUcsRUFBRSxDQUFDLFdBQVc7b0JBQ3JCLE9BQU8sRUFBRyxRQUFRO2lCQUNsQixDQUFDOytDQUdEO1lBVlcsUUFBUTtnQkFGcEIsT0FBTyxDQUFDLGtCQUFrQixDQUFDO2dCQUMzQixJQUFJLENBQUMsc0NBQXNDLENBQUM7ZUFDaEMsUUFBUSxDQStJcEI7WUFBRCxlQUFDO1NBL0lELEFBK0lDLENBL0k2QixFQUFFLENBQUMsU0FBUyxHQStJekM7UUEvSVksWUFBUSxXQStJcEIsQ0FBQTtJQUVELENBQUMsRUFwS2EsR0FBRyxHQUFILE9BQUcsS0FBSCxPQUFHLFFBb0toQjtBQUVELENBQUMsRUF4S00sR0FBRyxLQUFILEdBQUcsUUF3S1Q7QUN2S0QsSUFBTyxHQUFHLENBd0tUO0FBeEtELFdBQU8sR0FBRztJQUVWLElBQWMsR0FBRyxDQW9LaEI7SUFwS0QsV0FBYyxHQUFHO1FBRVgsSUFBQSxrQkFBNEQsRUFBM0Qsb0JBQU8sRUFBRSxzQkFBUSxFQUFFLHNDQUFnQixFQUFFLGNBQXNCLENBQUM7UUFLbkUsSUFBWSxNQUdYO1FBSEQsV0FBWSxNQUFNO1lBQ2QsMkNBQVksQ0FBQTtZQUNaLDJDQUFZLENBQUE7UUFDaEIsQ0FBQyxFQUhXLE1BQU0sR0FBTixVQUFNLEtBQU4sVUFBTSxRQUdqQjtRQUtELElBQVksTUFTWDtRQVRELFdBQVksTUFBTTtZQUNkLDZCQUFXLENBQUE7WUFDWCw2QkFBVyxDQUFBO1lBQ1gsdUNBQVcsQ0FBQTtZQUNYLHFDQUFXLENBQUE7WUFDWCwrQkFBVyxDQUFBO1lBQ1gsbUNBQVcsQ0FBQTtZQUNYLG1DQUFXLENBQUE7WUFDWCxxQ0FBVyxDQUFBO1FBQ2YsQ0FBQyxFQVRXLE1BQU0sR0FBTixVQUFNLEtBQU4sVUFBTSxRQVNqQjtRQUdEO1lBY0k7Z0JBQVksY0FBYTtxQkFBYixVQUFhLEVBQWIscUJBQWEsRUFBYixJQUFhO29CQUFiLHlCQUFhOztnQkFSekIsV0FBTSxHQUFVLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBTXpCLFFBQUcsR0FBaUIsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUcvQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDekMsQ0FBQztZQVhEO2dCQUxDLFFBQVEsQ0FBQztvQkFDTixJQUFJLEVBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ3RCLE9BQU8sRUFBRyxNQUFNO29CQUNoQixRQUFRLEVBQUcsSUFBSTtpQkFDbEIsQ0FBQztxREFDdUI7WUFNekI7Z0JBSkMsUUFBUSxDQUFDO29CQUNOLElBQUksRUFBRyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO29CQUM1QixPQUFPLEVBQUcsTUFBTTtpQkFDbkIsQ0FBQztrREFDaUM7WUFaMUIsU0FBUztnQkFEckIsT0FBTyxDQUFDLG1CQUFtQixDQUFDO2VBQ2hCLFNBQVMsQ0FrQnJCO1lBQUQsZ0JBQUM7U0FsQkQsQUFrQkMsSUFBQTtRQWxCWSxhQUFTLFlBa0JyQixDQUFBO1FBS0Q7WUFBZ0MsOEJBQVk7WUFBNUM7Z0JBQUEscUVBZ0hDO2dCQTFHRyxZQUFNLEdBQVUsTUFBTSxDQUFDLFFBQVEsQ0FBQztnQkFHaEMsY0FBUSxHQUFlO29CQUNuQixJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDdkMsSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZDLElBQUksU0FBUyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUM1QyxJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDM0MsSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3hDLElBQUksU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUMxQyxJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDMUMsSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzlDLENBQUM7Z0JBa0JNLFVBQUksR0FBTyxJQUFJLENBQUM7Z0JBS2hCLGFBQU8sR0FBeUIsSUFBSSxDQUFDOztZQXVFakQsQ0FBQztZQXpGRyxzQkFBSSwrQkFBTztxQkFBWDtvQkFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3pCLENBQUM7cUJBQ0QsVUFBWSxLQUFpQjtvQkFDL0IsSUFBRyxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssRUFBQzt3QkFDekIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7d0JBQ3RCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztxQkFDdkI7Z0JBQ0MsQ0FBQzs7O2VBTkE7WUFvQkQsMkJBQU0sR0FBTjtnQkFDRixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBQSxRQUFRLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDakQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN2QixFQUFFLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDM0UsRUFBRSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3ZFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUM3RCxDQUFDO1lBRUQsOEJBQVMsR0FBVDtnQkFDSSxFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUIsQ0FBQztZQVFPLHNDQUFpQixHQUF6QixVQUEwQixNQUFhLEVBQUUsTUFBYSxFQUFFLElBQVk7Z0JBQ2hFLElBQUcsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUM7b0JBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO2lCQUNwQztZQUNMLENBQUM7WUFLRCxrQ0FBYSxHQUFiLFVBQWMsTUFBYSxFQUFFLElBQVk7Z0JBQzNDLElBQUcsSUFBSSxFQUFDO29CQUNQLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7aUJBQzFDO3FCQUFJO29CQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7aUJBQ3hDO1lBQ0MsQ0FBQztZQUtPLDhCQUFTLEdBQWpCLFVBQWtCLEtBQTRCO2dCQUMxQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDekMsSUFBRyxNQUFNLElBQUksSUFBSSxFQUFDO29CQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDM0I7WUFDTCxDQUFDO1lBS08sNEJBQU8sR0FBZixVQUFnQixLQUE0QjtnQkFDeEMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3pDLElBQUcsTUFBTSxJQUFJLElBQUksRUFBQztvQkFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQzVCO1lBQ0wsQ0FBQztZQUtPLG9DQUFlLEdBQXZCO2dCQUNJLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO2dCQUNsQixLQUFjLFVBQWEsRUFBYixLQUFBLElBQUksQ0FBQyxRQUFRLEVBQWIsY0FBYSxFQUFiLElBQWEsRUFBQztvQkFBdkIsSUFBSSxDQUFDLFNBQUE7b0JBQ04sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztpQkFDbEM7WUFDTCxDQUFDO1lBdkdEO2dCQUpDLFFBQVEsQ0FBQztvQkFDTixJQUFJLEVBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ3RCLE9BQU8sRUFBRyxNQUFNO2lCQUNuQixDQUFDO3NEQUM4QjtZQUdoQztnQkFEQyxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQzt3REFVcEI7WUFLRjtnQkFKQyxRQUFRLENBQUM7b0JBQ04sSUFBSSxFQUFHLENBQUMsU0FBUyxDQUFDO29CQUNsQixPQUFPLEVBQUcsT0FBTztpQkFDcEIsQ0FBQztxREFHRDtZQXpCUSxVQUFVO2dCQUh0QixPQUFPLENBQUMsb0JBQW9CLENBQUM7Z0JBQzdCLGdCQUFnQixDQUFDLElBQUEsUUFBUSxDQUFDO2dCQUMxQixJQUFJLENBQUMsd0NBQXdDLENBQUM7ZUFDbEMsVUFBVSxDQWdIdEI7WUFBRCxpQkFBQztTQWhIRCxBQWdIQyxDQWhIK0IsRUFBRSxDQUFDLFNBQVMsR0FnSDNDO1FBaEhZLGNBQVUsYUFnSHRCLENBQUE7SUFFRCxDQUFDLEVBcEthLEdBQUcsR0FBSCxPQUFHLEtBQUgsT0FBRyxRQW9LaEI7QUFFRCxDQUFDLEVBeEtNLEdBQUcsS0FBSCxHQUFHLFFBd0tUO0FDeEtELElBQU8sR0FBRyxDQXlEVDtBQXpERCxXQUFPLEdBQUc7SUFFVixJQUFjLEdBQUcsQ0FxRGhCO0lBckRELFdBQWMsR0FBRztRQUVYLElBQUEsa0JBQTRELEVBQTNELG9CQUFPLEVBQUUsc0JBQVEsRUFBRSxzQ0FBZ0IsRUFBRSxjQUFzQixDQUFDO1FBS25FO1lBQW9DLGtDQUFZO1lBQWhEO2dCQUFBLHFFQTRDQztnQkF6Q0csY0FBUSxHQUFnQixFQUFFLENBQUM7Z0JBa0JuQixlQUFTLEdBQVksSUFBSSxDQUFDOztZQXVCdEMsQ0FBQztZQXBDQSxzQkFBSSxtQ0FBTztxQkFBWDtvQkFDQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3RCLENBQUM7cUJBQ0QsVUFBWSxLQUFpQjtvQkFDNUIsSUFBRyxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssRUFBQzt3QkFDekIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7d0JBQ3RCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDOUI7Z0JBQ0MsQ0FBQzs7O2VBTkg7WUFlRSwrQkFBTSxHQUFOO2dCQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFBLFFBQVEsQ0FBQyxDQUFDO2dCQUM3QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pDLENBQUM7WUFNTywwQ0FBaUIsR0FBekIsVUFBMEIsT0FBbUI7Z0JBQ3pDLElBQUcsT0FBTyxJQUFJLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO29CQUM3QixJQUFJLFdBQVcsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO29CQUNsRSxLQUFjLFVBQU8sRUFBUCxtQkFBTyxFQUFQLHFCQUFPLEVBQVAsSUFBTyxFQUFDO3dCQUFsQixJQUFJLElBQUUsZ0JBQUE7d0JBQ04sSUFBRSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7cUJBQ2hDO2lCQUNKO1lBQ0wsQ0FBQztZQXRDRDtnQkFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7NERBQ0s7WUFLOUI7Z0JBSkMsUUFBUSxDQUFDO29CQUNULElBQUksRUFBRyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7b0JBQ2xCLE9BQU8sRUFBRyxhQUFhO2lCQUN2QixDQUFDO3lEQUdEO1lBVlcsY0FBYztnQkFIMUIsT0FBTyxDQUFDLHdCQUF3QixDQUFDO2dCQUNqQyxnQkFBZ0IsQ0FBQyxJQUFBLFFBQVEsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLDRDQUE0QyxDQUFDO2VBQ3RDLGNBQWMsQ0E0QzFCO1lBQUQscUJBQUM7U0E1Q0QsQUE0Q0MsQ0E1Q21DLEVBQUUsQ0FBQyxTQUFTLEdBNEMvQztRQTVDWSxrQkFBYyxpQkE0QzFCLENBQUE7SUFFRCxDQUFDLEVBckRhLEdBQUcsR0FBSCxPQUFHLEtBQUgsT0FBRyxRQXFEaEI7QUFFRCxDQUFDLEVBekRNLEdBQUcsS0FBSCxHQUFHLFFBeURUIiwiZmlsZSI6ImxjYy1uZXMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJcclxubW9kdWxlIGxjYyB7XHJcblxyXG4vKipcclxuICog546v5b2i57yT5Yay5Yy6XHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgUmluZ0J1ZmZlciAge1xyXG5cdFxyXG5cdHByaXZhdGUgX2VsZW1lbnRzOmFueVtdID0gbnVsbDtcclxuXHRwcml2YXRlIF9maXJzdDpudW1iZXIgPSAwO1xyXG5cdHByaXZhdGUgX2xhc3Q6bnVtYmVyID0gMDtcclxuXHRwcml2YXRlIF9zaXplOm51bWJlciA9IDA7XHJcblx0cHJpdmF0ZSBfZXZpY3RlZENiOkZ1bmN0aW9uID0gbnVsbDtcclxuXHJcblx0LyoqXHJcblx0ICogSW5pdGlhbGl6ZXMgYSBuZXcgZW1wdHkgYFJpbmdCdWZmZXJgIHdpdGggdGhlIGdpdmVuIGBjYXBhY2l0eWAsIHdoZW4gbm9cclxuXHQgKiB2YWx1ZSBpcyBwcm92aWRlZCB1c2VzIHRoZSBkZWZhdWx0IGNhcGFjaXR5ICg1MCkuXHJcblx0ICpcclxuXHQgKiBJZiBwcm92aWRlZCwgYGV2aWN0ZWRDYmAgZ2V0cyBydW4gd2l0aCBhbnkgZXZpY3RlZCBlbGVtZW50cy5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7TnVtYmVyfVxyXG5cdCAqIEBwYXJhbSB7RnVuY3Rpb259XHJcblx0ICogQHJldHVybiB7UmluZ0J1ZmZlcn1cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdGNvbnN0cnVjdG9yKGNhcGFjaXR5Om51bWJlciwgZXZpY3RlZENiPzpGdW5jdGlvbil7XHJcblx0XHR0aGlzLl9lbGVtZW50cyA9IG5ldyBBcnJheShjYXBhY2l0eSB8fCA1MCk7XHJcblx0XHR0aGlzLl9maXJzdCA9IDA7XHJcblx0XHR0aGlzLl9sYXN0ID0gMDtcclxuXHRcdHRoaXMuX3NpemUgPSAwO1xyXG5cdFx0dGhpcy5fZXZpY3RlZENiID0gZXZpY3RlZENiO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogUmV0dXJucyB0aGUgY2FwYWNpdHkgb2YgdGhlIHJpbmcgYnVmZmVyLlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7TnVtYmVyfVxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0cHVibGljIGNhcGFjaXR5KCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fZWxlbWVudHMubGVuZ3RoO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogUmV0dXJucyB0aGUgc2l6ZSBvZiB0aGUgcXVldWUuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtOdW1iZXJ9XHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRwdWJsaWMgc2l6ZSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3NpemU7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBSZXR1cm5zIHdoZXRoZXIgdGhlIHJpbmcgYnVmZmVyIGlzIGVtcHR5IG9yIG5vdC5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm4ge0Jvb2xlYW59XHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRwdWJsaWMgaXNFbXB0eSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuc2l6ZSgpID09PSAwO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogUmV0dXJucyB3aGV0aGVyIHRoZSByaW5nIGJ1ZmZlciBpcyBmdWxsIG9yIG5vdC5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm4ge0Jvb2xlYW59XHJcblx0ICogQGFwaSBwdWJsaWNcclxuXHQgKi9cclxuXHRwdWJsaWMgaXNGdWxsKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5zaXplKCkgPT09IHRoaXMuY2FwYWNpdHkoKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFBlZWtzIGF0IHRoZSB0b3AgZWxlbWVudCBvZiB0aGUgcXVldWUuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtPYmplY3R9XHJcblx0ICogQHRocm93cyB7RXJyb3J9IHdoZW4gdGhlIHJpbmcgYnVmZmVyIGlzIGVtcHR5LlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0cHVibGljIHBlZWsoKXtcclxuXHRcdGlmICh0aGlzLmlzRW1wdHkoKSkgdGhyb3cgbmV3IEVycm9yKCdSaW5nQnVmZmVyIGlzIGVtcHR5Jyk7XHJcblx0XHRyZXR1cm4gdGhpcy5fZWxlbWVudHNbdGhpcy5fZmlyc3RdO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogUGVla3MgYXQgbXVsdGlwbGUgZWxlbWVudHMgaW4gdGhlIHF1ZXVlLlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7QXJyYXl9XHJcblx0ICogQHRocm93cyB7RXJyb3J9IHdoZW4gdGhlcmUgYXJlIG5vdCBlbm91Z2ggZWxlbWVudHMgaW4gdGhlIGJ1ZmZlci5cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdHB1YmxpYyBwZWVrTihjb3VudDpudW1iZXIpe1xyXG5cdFx0aWYgKGNvdW50ID4gdGhpcy5fc2l6ZSkgdGhyb3cgbmV3IEVycm9yKCdOb3QgZW5vdWdoIGVsZW1lbnRzIGluIFJpbmdCdWZmZXInKTtcclxuXHJcblx0XHRsZXQgZW5kID0gTWF0aC5taW4odGhpcy5fZmlyc3QgKyBjb3VudCwgdGhpcy5jYXBhY2l0eSgpKTtcclxuXHRcdGxldCBmaXJzdEhhbGYgPSB0aGlzLl9lbGVtZW50cy5zbGljZSh0aGlzLl9maXJzdCwgZW5kKTtcclxuXHRcdGlmIChlbmQgPCB0aGlzLmNhcGFjaXR5KCkpIHtcclxuXHRcdCAgXHRyZXR1cm4gZmlyc3RIYWxmO1xyXG5cdFx0fVxyXG5cdFx0bGV0IHNlY29uZEhhbGYgPSB0aGlzLl9lbGVtZW50cy5zbGljZSgwLCBjb3VudCAtIGZpcnN0SGFsZi5sZW5ndGgpO1xyXG5cdFx0cmV0dXJuIGZpcnN0SGFsZi5jb25jYXQoc2Vjb25kSGFsZik7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBEZXF1ZXVlcyB0aGUgdG9wIGVsZW1lbnQgb2YgdGhlIHF1ZXVlLlxyXG5cdCAqXHJcblx0ICogQHJldHVybiB7T2JqZWN0fVxyXG5cdCAqIEB0aHJvd3Mge0Vycm9yfSB3aGVuIHRoZSByaW5nIGJ1ZmZlciBpcyBlbXB0eS5cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdHB1YmxpYyBkZXEoKXtcclxuXHRcdGxldCBlbGVtZW50ID0gdGhpcy5wZWVrKCk7XHJcblxyXG5cdFx0dGhpcy5fc2l6ZS0tO1xyXG5cdFx0dGhpcy5fZmlyc3QgPSAodGhpcy5fZmlyc3QgKyAxKSAlIHRoaXMuY2FwYWNpdHkoKTtcclxuXHQgIFxyXG5cdFx0cmV0dXJuIGVsZW1lbnQ7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBEZXF1ZXVlcyBtdWx0aXBsZSBlbGVtZW50cyBvZiB0aGUgcXVldWUuXHJcblx0ICpcclxuXHQgKiBAcmV0dXJuIHtBcnJheX1cclxuXHQgKiBAdGhyb3dzIHtFcnJvcn0gd2hlbiB0aGVyZSBhcmUgbm90IGVub3VnaCBlbGVtZW50cyBpbiB0aGUgYnVmZmVyLlxyXG5cdCAqIEBhcGkgcHVibGljXHJcblx0ICovXHJcblx0cHVibGljIGRlcU4oY291bnQ6bnVtYmVyKXtcclxuXHRcdGxldCBlbGVtZW50cyA9IHRoaXMucGVla04oY291bnQpO1xyXG5cclxuXHRcdHRoaXMuX3NpemUgLT0gY291bnQ7XHJcblx0XHR0aGlzLl9maXJzdCA9ICh0aGlzLl9maXJzdCArIGNvdW50KSAlIHRoaXMuY2FwYWNpdHkoKTtcclxuXHQgIFxyXG5cdFx0cmV0dXJuIGVsZW1lbnRzO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogRW5xdWV1ZXMgdGhlIGBlbGVtZW50YCBhdCB0aGUgZW5kIG9mIHRoZSByaW5nIGJ1ZmZlciBhbmQgcmV0dXJucyBpdHMgbmV3IHNpemUuXHJcblx0ICpcclxuXHQgKiBAcGFyYW0ge09iamVjdH0gZWxlbWVudFxyXG5cdCAqIEByZXR1cm4ge051bWJlcn1cclxuXHQgKiBAYXBpIHB1YmxpY1xyXG5cdCAqL1xyXG5cdHB1YmxpYyBlbnEoZWxlbWVudDphbnkpe1xyXG5cdFx0dGhpcy5fbGFzdCA9ICh0aGlzLl9maXJzdCArIHRoaXMuc2l6ZSgpKSAlIHRoaXMuY2FwYWNpdHkoKTtcclxuXHRcdGxldCBmdWxsID0gdGhpcy5pc0Z1bGwoKVxyXG5cdFx0aWYgKGZ1bGwgJiYgdGhpcy5fZXZpY3RlZENiKSB7XHJcblx0XHQgIHRoaXMuX2V2aWN0ZWRDYih0aGlzLl9lbGVtZW50c1t0aGlzLl9sYXN0XSk7XHJcblx0XHR9XHJcblx0XHR0aGlzLl9lbGVtZW50c1t0aGlzLl9sYXN0XSA9IGVsZW1lbnQ7XHJcblx0ICBcclxuXHRcdGlmIChmdWxsKSB7XHJcblx0XHQgIHRoaXMuX2ZpcnN0ID0gKHRoaXMuX2ZpcnN0ICsgMSkgJSB0aGlzLmNhcGFjaXR5KCk7XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0ICB0aGlzLl9zaXplKys7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdHJldHVybiB0aGlzLnNpemUoKTtcclxuXHR9XHJcbn1cclxuXHJcbn1cclxuIiwiXHJcbm1vZHVsZSBsY2Mge1xyXG5cclxuZXhwb3J0IG1vZHVsZSBVdGlscyB7XHJcblxyXG4vKipcclxuICogQXJyYXlCdWZmZXIg6L2sIGJpbmFyeSBzdHJpbmdcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBhYjJicyhhYjpBcnJheUJ1ZmZlcil7XHJcblx0cmV0dXJuIG5ldyBQcm9taXNlPHN0cmluZz4oKHJlc29sdmUpPT57XHJcblx0XHR2YXIgYiA9IG5ldyBCbG9iKFthYl0pO1xyXG5cdFx0dmFyIHIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG5cdFx0ci5yZWFkQXNCaW5hcnlTdHJpbmcoYik7XHJcblx0XHRyLm9ubG9hZCA9ICgpPT57XHJcblx0XHRcdHJlc29sdmUoci5yZXN1bHQgYXMgc3RyaW5nKTtcclxuXHRcdH1cclxuXHR9KTtcclxufVxyXG5cclxufVxyXG5cclxufVxyXG4iLCJcclxubW9kdWxlIGxjYyB7XHJcblxyXG5leHBvcnQgbW9kdWxlIG5lcyB7XHJcblx0XHRcdFxyXG5jb25zdCBCVUZGRVJTSVpFID0gODE5MjtcclxuXHJcbi8qKlxyXG4gKiDpn7PpopHmkq3mlL7lmahcclxuICovXHJcbmV4cG9ydCBjbGFzcyBBdWRpb1BsYXllciB7XHJcblxyXG5cdC8qKlxyXG5cdCAqIOe8k+WGsuWMulxyXG5cdCAqL1xyXG5cdHByaXZhdGUgX2J1ZmZlcjpSaW5nQnVmZmVyID0gbnVsbDtcclxuXHJcblx0cHJpdmF0ZSBfYXVkaW9DdHg6QXVkaW9Db250ZXh0ID0gbnVsbDtcclxuXHRwcml2YXRlIF9zY3JpcHROb2RlOlNjcmlwdFByb2Nlc3Nvck5vZGUgPSBudWxsO1xyXG5cclxuXHRjb25zdHJ1Y3Rvcigpe1xyXG5cdFx0dGhpcy5fYnVmZmVyID0gbmV3IFJpbmdCdWZmZXIoQlVGRkVSU0laRSAqIDIpO1xyXG5cdH1cclxuXHRcclxuXHRnZXRTYW1wbGVSYXRlKCkge1xyXG5cdFx0aWYgKCFBdWRpb0NvbnRleHQpIHtcclxuXHRcdCAgcmV0dXJuIDQ0MTAwO1xyXG5cdFx0fVxyXG5cdFx0bGV0IG15Q3R4ID0gbmV3IEF1ZGlvQ29udGV4dCgpO1xyXG5cdFx0bGV0IHNhbXBsZVJhdGUgPSBteUN0eC5zYW1wbGVSYXRlO1xyXG5cdFx0bXlDdHguY2xvc2UoKTtcclxuXHRcdHJldHVybiBzYW1wbGVSYXRlO1xyXG5cdH1cclxuXHJcblx0c3RhcnQoKSB7XHJcblx0XHRpZighdGhpcy5fc2NyaXB0Tm9kZSl7XHJcblx0XHRcdC8vIEF1ZGlvIGlzIG5vdCBzdXBwb3J0ZWRcclxuXHRcdFx0aWYgKCFBdWRpb0NvbnRleHQpIHtcclxuXHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH1cclxuXHRcdFx0dGhpcy5fYXVkaW9DdHggPSBuZXcgQXVkaW9Db250ZXh0KCk7XHJcblx0XHRcdHRoaXMuX3NjcmlwdE5vZGUgPSB0aGlzLl9hdWRpb0N0eC5jcmVhdGVTY3JpcHRQcm9jZXNzb3IoMTAyNCwgMCwgMik7XHJcblx0XHRcdHRoaXMuX3NjcmlwdE5vZGUub25hdWRpb3Byb2Nlc3MgPSB0aGlzLm9uYXVkaW9wcm9jZXNzLmJpbmQodGhpcyk7XHJcblx0XHRcdHRoaXMuX3NjcmlwdE5vZGUuY29ubmVjdCh0aGlzLl9hdWRpb0N0eC5kZXN0aW5hdGlvbik7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdGFzeW5jIHN0b3AoKSB7XHJcblx0XHRpZiAodGhpcy5fc2NyaXB0Tm9kZSkge1xyXG5cdFx0ICBcdHRoaXMuX3NjcmlwdE5vZGUuZGlzY29ubmVjdCh0aGlzLl9hdWRpb0N0eC5kZXN0aW5hdGlvbik7XHJcblx0XHQgIFx0dGhpcy5fc2NyaXB0Tm9kZS5vbmF1ZGlvcHJvY2VzcyA9IG51bGw7XHJcblx0XHQgIFx0dGhpcy5fc2NyaXB0Tm9kZSA9IG51bGw7XHJcblx0XHR9XHJcblx0XHRpZiAodGhpcy5fYXVkaW9DdHgpIHtcclxuXHRcdCAgXHRhd2FpdCB0aGlzLl9hdWRpb0N0eC5jbG9zZSgpO1xyXG5cdFx0ICBcdHRoaXMuX2F1ZGlvQ3R4ID0gbnVsbDtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHdyaXRlU2FtcGxlKGxlZnQsIHJpZ2h0KSB7XHJcblx0XHRpZiAodGhpcy5fYnVmZmVyLnNpemUoKSAvIDIgPj0gQlVGRkVSU0laRSkge1xyXG5cdFx0ICBcdC8vY29uc29sZS5sb2coYEJ1ZmZlciBvdmVycnVuYCk7XHJcblx0XHQgIFx0dGhpcy5fYnVmZmVyLmRlcU4oQlVGRkVSU0laRSAvIDIpO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy5fYnVmZmVyLmVucShsZWZ0KTtcclxuXHRcdHRoaXMuX2J1ZmZlci5lbnEocmlnaHQpO1xyXG5cdH07XHJcblx0XHJcblx0b25hdWRpb3Byb2Nlc3MoZSkge1xyXG5cdFx0dmFyIGxlZnQgPSBlLm91dHB1dEJ1ZmZlci5nZXRDaGFubmVsRGF0YSgwKTtcclxuXHRcdHZhciByaWdodCA9IGUub3V0cHV0QnVmZmVyLmdldENoYW5uZWxEYXRhKDEpO1xyXG5cdFx0dmFyIHNpemUgPSBsZWZ0Lmxlbmd0aDtcclxuXHRcdFxyXG5cdFx0Ly8gV2UncmUgZ29pbmcgdG8gYnVmZmVyIHVuZGVycnVuLiBBdHRlbXB0IHRvIGZpbGwgdGhlIGJ1ZmZlci5cclxuXHRcdC8vaWYgKHRoaXMuX2J1ZmZlci5zaXplKCkgPCBzaXplICogMiAmJiB0aGlzLm9uQnVmZmVyVW5kZXJydW4pIHtcclxuXHRcdC8vICBcdHRoaXMub25CdWZmZXJVbmRlcnJ1bih0aGlzLl9idWZmZXIuc2l6ZSgpLCBzaXplICogMik7XHJcblx0XHQvL31cclxuXHRcdFxyXG5cdFx0dHJ5IHtcclxuXHRcdCAgXHR2YXIgc2FtcGxlcyA9IHRoaXMuX2J1ZmZlci5kZXFOKHNpemUgKiAyKTtcclxuXHRcdH0gY2F0Y2ggKGUpIHtcclxuXHRcdFx0Ly8gb25CdWZmZXJVbmRlcnJ1biBmYWlsZWQgdG8gZmlsbCB0aGUgYnVmZmVyLCBzbyBoYW5kbGUgYSByZWFsIGJ1ZmZlclxyXG5cdFx0XHQvLyB1bmRlcnJ1blxyXG5cdFx0XHJcblx0XHRcdC8vIGlnbm9yZSBlbXB0eSBidWZmZXJzLi4uIGFzc3VtZSBhdWRpbyBoYXMganVzdCBzdG9wcGVkXHJcblx0XHRcdHZhciBidWZmZXJTaXplID0gdGhpcy5fYnVmZmVyLnNpemUoKSAvIDI7XHJcblx0XHRcdGlmIChidWZmZXJTaXplID4gMCkge1xyXG5cdFx0XHRcdC8vY29uc29sZS5sb2coYEJ1ZmZlciB1bmRlcnJ1biAobmVlZGVkICR7c2l6ZX0sIGdvdCAke2J1ZmZlclNpemV9KWApO1xyXG5cdFx0XHR9XHJcblx0XHRcdGZvciAodmFyIGogPSAwOyBqIDwgc2l6ZTsgaisrKSB7XHJcblx0XHRcdFx0bGVmdFtqXSA9IDA7XHJcblx0XHRcdFx0cmlnaHRbal0gPSAwO1xyXG5cdFx0XHR9XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgc2l6ZTsgaSsrKSB7XHJcblx0XHQgIFx0bGVmdFtpXSA9IHNhbXBsZXNbaSAqIDJdO1xyXG5cdFx0ICBcdHJpZ2h0W2ldID0gc2FtcGxlc1tpICogMiArIDFdO1xyXG5cdFx0fVxyXG5cdH07XHJcbn1cclxuXHJcbn1cclxuXHJcbn1cclxuIiwiXHJcbm1vZHVsZSBsY2Mge1xyXG5cclxuZXhwb3J0IG1vZHVsZSBuZXMge1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5LCBtZW51IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuLyoqXHJcbiAqIGpzbmVz5bqTXHJcbiAqL1xyXG5kZWNsYXJlIGxldCBqc25lczphbnk7XHJcblxyXG4vKipcclxuICog5bin5aSn5bCPXHJcbiAqL1xyXG5jb25zdCBGUkFNRVNJWkUgPSBjYy5zaXplKDI1NiwyNDApO1xyXG5cclxuLy9AdHMtaWdub3JlXHJcbmxldCBnZnggPSBjYy5nZng7XHJcblxyXG5AY2NjbGFzcyhcImxjYy5uZXMuRW11bGF0b3JcIilcclxuQG1lbnUoXCJpMThuOmxjYy1uZXMubWVudV9jb21wb25lbnQvRW11bGF0b3JcIilcclxuZXhwb3J0IGNsYXNzIEVtdWxhdG9yIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuQnVmZmVyQXNzZXQpXHJcbiAgICBfcm9tOiBjYy5CdWZmZXJBc3NldCA9IG51bGw7XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBjYy5CdWZmZXJBc3NldCxcclxuXHRcdHRvb2x0aXAgOiBcIlJPTSDmlbDmja5cIlxyXG5cdH0pXHJcblx0Z2V0IHJvbSgpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3JvbTtcclxuXHR9XHJcblx0c2V0IHJvbSh2YWx1ZTpjYy5CdWZmZXJBc3NldCl7XHJcblx0XHRpZih0aGlzLl9yb20gIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9yb20gPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy5zZXRSb21EYXRhKHZhbHVlKTtcclxuXHRcdH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIOWHhuWkh+WlvVxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIF9wcmVwYXJlOmJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIE5FUyDlr7nosaFcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBfbmVzOmFueSA9IG51bGw7XHJcbiAgICBcclxuICAgIC8qKlxyXG4gICAgICog57q555CG5a+56LGhXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgX3RleHR1cmU6Y2MuVGV4dHVyZTJEID0gbnVsbDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIOe6ueeQhue8k+WGslxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIF9mcmFtZWJ1ZmY6QXJyYXlCdWZmZXIgPSBudWxsO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICog57yT5YayIFVpbnQ46KeG5Zu+XHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgX2ZyYW1ldTg6VWludDhBcnJheSA9IG51bGw7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDnvJPlhrIgVWludDMy6KeG5Zu+XHJcbiAgICAgKi9cclxuXHRwcml2YXRlIF9mcmFtZXUzMjpVaW50MzJBcnJheSA9IG51bGw7XHJcblx0XHJcblx0LyoqXHJcblx0ICog6Z+z6aKR5pKt5pS+5ZmoXHJcblx0ICovXHJcblx0cHJpdmF0ZSBfYXVkaW86QXVkaW9QbGF5ZXIgPSBudWxsO1xyXG5cclxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuICAgIG9uTG9hZCAoKSB7XHJcblx0XHR0aGlzLl9hdWRpbyA9IG5ldyBBdWRpb1BsYXllcigpO1xyXG5cdFx0dGhpcy5fbmVzID0gbmV3IGpzbmVzLk5FUyh7XHJcblx0XHRcdG9uRnJhbWUgOiB0aGlzLnNldEZyYW1lQnVmZmVyLmJpbmQodGhpcyksXHJcblx0XHRcdG9uU3RhdHVzVXBkYXRlIDogY2MubG9nLFxyXG5cdFx0XHRzYW1wbGVSYXRlIDogdGhpcy5fYXVkaW8uZ2V0U2FtcGxlUmF0ZSgpLFxyXG5cdFx0XHRvbkF1ZGlvU2FtcGxlIDogdGhpcy5fYXVkaW8ud3JpdGVTYW1wbGUuYmluZCh0aGlzLl9hdWRpbyksXHJcblx0XHR9KTtcclxuXHRcdHRoaXMuX3RleHR1cmUgPSBuZXcgY2MuVGV4dHVyZTJEKCk7XHJcblx0XHR0aGlzLl9mcmFtZWJ1ZmYgPSBuZXcgQXJyYXlCdWZmZXIoRlJBTUVTSVpFLndpZHRoICogRlJBTUVTSVpFLmhlaWdodCAqIDQpO1xyXG5cdFx0dGhpcy5fZnJhbWV1OCA9IG5ldyBVaW50OEFycmF5KHRoaXMuX2ZyYW1lYnVmZik7XHJcblx0XHR0aGlzLl9mcmFtZXUzMiA9IG5ldyBVaW50MzJBcnJheSh0aGlzLl9mcmFtZWJ1ZmYpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog6K6+572u5bin57yT5YayXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgc2V0RnJhbWVCdWZmZXIoYnVmZmVyOkFycmF5QnVmZmVyKXtcclxuICAgICAgICBsZXQgaSA9IDA7XHJcbiAgICAgICAgZm9yIChsZXQgeSA9IDA7IHkgPCBGUkFNRVNJWkUuaGVpZ2h0OyArK3kpIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgeCA9IDA7IHggPCBGUkFNRVNJWkUud2lkdGg7ICsreCkge1xyXG4gICAgICAgICAgICAgICAgaSA9IHkgKiAyNTYgKyB4O1xyXG4gICAgICAgICAgICAgICAgLy8gQ29udmVydCBwaXhlbCBmcm9tIE5FUyBCR1IgdG8gY2FudmFzIEFCR1JcclxuICAgICAgICAgICAgICAgIHRoaXMuX2ZyYW1ldTMyW2ldID0gMHhmZjAwMDAwMCB8IGJ1ZmZlcltpXTsgLy8gRnVsbCBhbHBoYVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIEB0cy1pZ25vcmVcclxuICAgICAgICB0aGlzLl90ZXh0dXJlLmluaXRXaXRoRGF0YSh0aGlzLl9mcmFtZXU4LCBnZnguVEVYVFVSRV9GTVRfUkdCQTgsIEZSQU1FU0laRS53aWR0aCwgRlJBTUVTSVpFLmhlaWdodCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDojrflvpdORVPlr7nosaFcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldE5FUygpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9uZXM7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC8qKlxyXG4gICAgICog6I635b6X57q555CG5a+56LGhXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRUZXh0dXJlKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RleHR1cmU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDorr7nva5ST03mlbDmja5cclxuICAgICAqL1xyXG4gICAgcHVibGljIGFzeW5jIHNldFJvbURhdGEoZGF0YTpjYy5CdWZmZXJBc3NldCl7XHJcblx0XHRpZihkYXRhKXtcclxuXHRcdFx0Ly8gQHRzLWlnbm9yZVxyXG5cdFx0XHR0aGlzLl9uZXMubG9hZFJPTShhd2FpdCBVdGlscy5hYjJicyhkYXRhLl9idWZmZXIpKTtcclxuXHRcdFx0dGhpcy5fYXVkaW8uc3RhcnQoKTtcclxuXHRcdFx0dGhpcy5fcHJlcGFyZSA9IHRydWU7XHJcblx0XHR9ZWxzZXtcclxuXHRcdFx0dGhpcy5fYXVkaW8uc3RvcCgpO1xyXG5cdFx0XHR0aGlzLl9wcmVwYXJlID0gZmFsc2U7XHJcblx0XHR9XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKGR0KXtcclxuICAgICAgICBpZih0aGlzLl9wcmVwYXJlKXtcclxuICAgICAgICAgICAgdGhpcy5fbmVzLmZyYW1lKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXJ0ICgpIHtcclxuICAgICAgICBpZih0aGlzLnJvbSl7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0Um9tRGF0YSh0aGlzLnJvbSk7XHJcbiAgICAgICAgfVxyXG5cdH1cclxuXHRcclxuXHQvKipcclxuXHQgKiDkv53lrZjlrZjmoaNcclxuXHQgKiBAcmV0dXJucyDlrZjmoaNKU09O5pWw5o2uXHJcblx0ICovXHJcblx0cHVibGljIHNhdmVBcmNoaXZlKCl7XHJcblx0XHRyZXR1cm4gdGhpcy5fbmVzLnNhdmVBcmNoaXZlKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiDliqDovb3lrZjmoaNcclxuXHQgKiBAcGFyYW0ganNvbiDlrZjmoaNKU09O5pWw5o2uXHJcblx0ICovXHJcblx0cHVibGljIGxvYWRBcmNoaXZlKGpzb24pe1xyXG5cdFx0dGhpcy5fbmVzLmxvYWRBcmNoaXZlKGpzb24pO1xyXG5cdH1cclxuICAgIFxyXG4gICAgLy8gdXBkYXRlIChkdCkge31cclxufVxyXG5cclxufVxyXG5cclxufVxyXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuL0VtdWxhdG9yLnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2Mge1xyXG5cclxuZXhwb3J0IG1vZHVsZSBuZXMge1xyXG5cdFx0XHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eSwgcmVxdWlyZUNvbXBvbmVudCwgbWVudSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbi8qKlxyXG4gKiDnjqnlrrbluo/lj7dcclxuICovXHJcbmV4cG9ydCBlbnVtIFBsYXllciB7XHJcbiAgICBQTEFZRVJfMSA9IDEsXHJcbiAgICBQTEFZRVJfMiA9IDIsXHJcbn1cclxuXHJcbi8qKlxyXG4gKiDmuLjmiI/mjInpkq5cclxuICovXHJcbmV4cG9ydCBlbnVtIEJ1dHRvbiB7XHJcbiAgICBBICAgICAgID0gMCxcclxuICAgIEIgICAgICAgPSAxLFxyXG4gICAgU0VMRUNUICA9IDIsXHJcbiAgICBTVEFSVCAgID0gMyxcclxuICAgIFVQICAgICAgPSA0LFxyXG4gICAgRE9XTiAgICA9IDUsXHJcbiAgICBMRUZUICAgID0gNixcclxuICAgIFJJR0hUICAgPSA3LFxyXG59XHJcblxyXG5AY2NjbGFzcyhcImxjYy5uZXMuQnV0dG9uTWFwXCIpXHJcbmV4cG9ydCBjbGFzcyBCdXR0b25NYXAge1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlIDogY2MuRW51bShCdXR0b24pLFxyXG4gICAgICAgIHRvb2x0aXAgOiBcIua4uOaIj+aMiemSrlwiLFxyXG4gICAgICAgIHJlYWRvbmx5IDogdHJ1ZSxcclxuICAgIH0pXHJcbiAgICBidXR0b246QnV0dG9uID0gQnV0dG9uLkE7XHJcblxyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlIDogY2MuRW51bShjYy5tYWNyby5LRVkpLFxyXG4gICAgICAgIHRvb2x0aXAgOiBcIumUruebmOaMiemUrlwiXHJcbiAgICB9KVxyXG4gICAga2V5OiBjYy5tYWNyby5LRVkgPSBjYy5tYWNyby5LRVkudztcclxuXHJcbiAgICBjb25zdHJ1Y3RvciguLi5hcmdzOmFueVtdKXtcclxuICAgICAgICB0aGlzLmJ1dHRvbiA9IGFyZ3NbMF0gfHwgQnV0dG9uLkE7XHJcbiAgICAgICAgdGhpcy5rZXkgPSBhcmdzWzFdIHx8IGNjLm1hY3JvLktFWS53O1xyXG4gICAgfVxyXG59XHJcblxyXG5AY2NjbGFzcyhcImxjYy5uZXMuQ29udHJvbGxlclwiKVxyXG5AcmVxdWlyZUNvbXBvbmVudChFbXVsYXRvcilcclxuQG1lbnUoXCJpMThuOmxjYy1uZXMubWVudV9jb21wb25lbnQvQ29udHJvbGxlclwiKVxyXG5leHBvcnQgY2xhc3MgQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlIDogY2MuRW51bShQbGF5ZXIpLFxyXG4gICAgICAgIHRvb2x0aXAgOiBcIueOqeWutuW6j+WPt1wiXHJcbiAgICB9KVxyXG4gICAgcGxheWVyOlBsYXllciA9IFBsYXllci5QTEFZRVJfMTtcclxuXHJcbiAgICBAcHJvcGVydHkoW0J1dHRvbk1hcF0pXHJcbiAgICBfYnV0dG9uczpCdXR0b25NYXBbXSA9IFtcclxuICAgICAgICBuZXcgQnV0dG9uTWFwKEJ1dHRvbi5BLCBjYy5tYWNyby5LRVkuaiksXHJcbiAgICAgICAgbmV3IEJ1dHRvbk1hcChCdXR0b24uQiwgY2MubWFjcm8uS0VZLmspLFxyXG4gICAgICAgIG5ldyBCdXR0b25NYXAoQnV0dG9uLlNFTEVDVCwgY2MubWFjcm8uS0VZLmYpLFxyXG4gICAgICAgIG5ldyBCdXR0b25NYXAoQnV0dG9uLlNUQVJULCBjYy5tYWNyby5LRVkuaCksXHJcbiAgICAgICAgbmV3IEJ1dHRvbk1hcChCdXR0b24uVVAsIGNjLm1hY3JvLktFWS53KSxcclxuICAgICAgICBuZXcgQnV0dG9uTWFwKEJ1dHRvbi5ET1dOLCBjYy5tYWNyby5LRVkucyksXHJcbiAgICAgICAgbmV3IEJ1dHRvbk1hcChCdXR0b24uTEVGVCwgY2MubWFjcm8uS0VZLmEpLFxyXG4gICAgICAgIG5ldyBCdXR0b25NYXAoQnV0dG9uLlJJR0hULCBjYy5tYWNyby5LRVkuZClcclxuICAgIF07XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGUgOiBbQnV0dG9uTWFwXSxcclxuICAgICAgICB0b29sdGlwIDogXCLmjInplK7mmKDlsITooahcIlxyXG4gICAgfSlcclxuICAgIGdldCBidXR0b25zKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2J1dHRvbnM7XHJcbiAgICB9IFxyXG4gICAgc2V0IGJ1dHRvbnModmFsdWU6QnV0dG9uTWFwW10pe1xyXG5cdFx0aWYodGhpcy5fYnV0dG9ucyAhPSB2YWx1ZSl7XHJcblx0XHRcdHRoaXMuX2J1dHRvbnMgPSB2YWx1ZTtcclxuXHRcdFx0dGhpcy51cGRhdGVCdXR0b25NYXAoKTtcclxuXHRcdH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE5FUyDlr7nosaFcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBfbmVzOmFueSA9IG51bGw7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDplK7nm5jmmKDlsIRcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBfa2V5bWFwOntba2V5Om51bWJlcl06bnVtYmVyfSA9IG51bGw7XHJcblxyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgb25Mb2FkICgpIHtcclxuXHRcdHRoaXMuX25lcyA9IHRoaXMuZ2V0Q29tcG9uZW50KEVtdWxhdG9yKS5nZXRORVMoKTtcclxuXHRcdHRoaXMudXBkYXRlQnV0dG9uTWFwKCk7XHJcblx0XHRjYy5zeXN0ZW1FdmVudC5vbihjYy5TeXN0ZW1FdmVudC5FdmVudFR5cGUuS0VZX0RPV04sIHRoaXMub25LZXlEb3duLCB0aGlzKTtcclxuXHRcdGNjLnN5c3RlbUV2ZW50Lm9uKGNjLlN5c3RlbUV2ZW50LkV2ZW50VHlwZS5LRVlfVVAsIHRoaXMub25LZXlVcCwgdGhpcyk7XHJcblx0XHR0aGlzLm5vZGUub24oXCJuZXNfYnV0dG9uX2V2ZW50XCIsIHRoaXMub25Ob2RlQnV0dG9uRXZlbnQsIHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRGVzdHJveSgpe1xyXG4gICAgICAgIGNjLnN5c3RlbUV2ZW50LnRhcmdldE9mZih0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUudGFyZ2V0T2ZmKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog6IqC54K55oyJ6ZKu5LqL5Lu2XHJcbiAgICAgKiBAcGFyYW0gcGxheWVyIFxyXG4gICAgICogQHBhcmFtIGJ1dHRvbiBcclxuICAgICAqIEBwYXJhbSBkb3duIFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIG9uTm9kZUJ1dHRvbkV2ZW50KHBsYXllcjpQbGF5ZXIsIGJ1dHRvbjpCdXR0b24sIGRvd246Ym9vbGVhbil7XHJcbiAgICAgICAgaWYocGxheWVyID09IHRoaXMucGxheWVyKXtcclxuICAgICAgICAgICAgdGhpcy5vbkJ1dHRvbkV2ZW50KGJ1dHRvbiwgZG93bik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5oyJ6ZKu5LqL5Lu2XHJcbiAgICAgKi9cclxuICAgIG9uQnV0dG9uRXZlbnQoYnV0dG9uOkJ1dHRvbiwgZG93bjpib29sZWFuKXtcclxuXHRcdGlmKGRvd24pe1xyXG5cdFx0XHR0aGlzLl9uZXMuYnV0dG9uRG93bih0aGlzLnBsYXllciwgYnV0dG9uKTtcclxuXHRcdH1lbHNle1xyXG5cdFx0XHR0aGlzLl9uZXMuYnV0dG9uVXAodGhpcy5wbGF5ZXIsIGJ1dHRvbik7XHJcblx0XHR9XHJcbiAgICB9XHJcblx0XHJcbiAgICAvKipcclxuICAgICAqIOW9k+aMiemSruaMieS4i1xyXG4gICAgICovXHJcbiAgICBwcml2YXRlIG9uS2V5RG93bihldmVudDpjYy5FdmVudC5FdmVudEtleWJvYXJkKXtcclxuICAgICAgICBsZXQgYnV0dG9uID0gdGhpcy5fa2V5bWFwW2V2ZW50LmtleUNvZGVdO1xyXG4gICAgICAgIGlmKGJ1dHRvbiAhPSBudWxsKXtcclxuXHRcdFx0dGhpcy5vbkJ1dHRvbkV2ZW50KGJ1dHRvbiwgdHJ1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5b2T5oyJ6ZKu5pS+5byAXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgb25LZXlVcChldmVudDpjYy5FdmVudC5FdmVudEtleWJvYXJkKXtcclxuICAgICAgICBsZXQgYnV0dG9uID0gdGhpcy5fa2V5bWFwW2V2ZW50LmtleUNvZGVdO1xyXG4gICAgICAgIGlmKGJ1dHRvbiAhPSBudWxsKXtcclxuXHRcdFx0dGhpcy5vbkJ1dHRvbkV2ZW50KGJ1dHRvbiwgZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIOabtOaWsOaMiemSruaYoOWwhFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIHVwZGF0ZUJ1dHRvbk1hcCgpe1xyXG4gICAgICAgIHRoaXMuX2tleW1hcCA9IHt9O1xyXG4gICAgICAgIGZvciAobGV0IGIgb2YgdGhpcy5fYnV0dG9ucyl7XHJcbiAgICAgICAgICAgIHRoaXMuX2tleW1hcFtiLmtleV0gPSBiLmJ1dHRvbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdXBkYXRlIChkdCkge31cclxufVxyXG5cclxufVxyXG5cclxufVxyXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID0gXCIuL0VtdWxhdG9yLnRzXCIgLz5cclxuXHJcbm1vZHVsZSBsY2Mge1xyXG5cclxuZXhwb3J0IG1vZHVsZSBuZXMge1xyXG5cdFxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHksIHJlcXVpcmVDb21wb25lbnQsIG1lbnUgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzcyhcImxjYy5uZXMuRGlzcGxheVNwcml0ZXNcIilcclxuQHJlcXVpcmVDb21wb25lbnQoRW11bGF0b3IpXHJcbkBtZW51KFwiaTE4bjpsY2MtbmVzLm1lbnVfY29tcG9uZW50L0Rpc3BsYXlTcHJpdGVzXCIpXHJcbmV4cG9ydCBjbGFzcyBEaXNwbGF5U3ByaXRlcyBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KFtjYy5TcHJpdGVdKVxyXG4gICAgX3Nwcml0ZXM6IGNjLlNwcml0ZVtdID0gW107XHJcblx0QHByb3BlcnR5KHtcclxuXHRcdHR5cGUgOiBbY2MuU3ByaXRlXSxcclxuXHRcdHRvb2x0aXAgOiBcIuWxleekuueahFNwcml0ZeaVsOe7hFwiXHJcblx0fSlcclxuXHRnZXQgc3ByaXRlcygpe1xyXG5cdFx0cmV0dXJuIHRoaXMuX3Nwcml0ZXM7XHJcblx0fVxyXG5cdHNldCBzcHJpdGVzKHZhbHVlOmNjLlNwcml0ZVtdKXtcclxuXHRcdGlmKHRoaXMuX3Nwcml0ZXMgIT0gdmFsdWUpe1xyXG5cdFx0XHR0aGlzLl9zcHJpdGVzID0gdmFsdWU7XHJcblx0XHRcdHRoaXMuc2V0RGlzcGxheVNwcml0ZXModmFsdWUpO1xyXG5cdFx0fVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5qih5ouf5ZmoXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgX2VtdWxhdG9yOkVtdWxhdG9yID0gbnVsbDtcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkxvYWQgKCkge1xyXG4gICAgICAgIHRoaXMuX2VtdWxhdG9yID0gdGhpcy5nZXRDb21wb25lbnQoRW11bGF0b3IpO1xyXG4gICAgICAgIHRoaXMuc2V0RGlzcGxheVNwcml0ZXModGhpcy5zcHJpdGVzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIOiuvue9ruWxleekulNwcml0ZeaVsOe7hFxyXG4gICAgICogQHBhcmFtIHNwcml0ZXMgXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgc2V0RGlzcGxheVNwcml0ZXMoc3ByaXRlczpjYy5TcHJpdGVbXSl7XHJcbiAgICAgICAgaWYoc3ByaXRlcyAmJiBzcHJpdGVzLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICBsZXQgc3ByaXRlRnJhbWUgPSBuZXcgY2MuU3ByaXRlRnJhbWUodGhpcy5fZW11bGF0b3IuZ2V0VGV4dHVyZSgpKTtcclxuICAgICAgICAgICAgZm9yKGxldCBzcCBvZiBzcHJpdGVzKXtcclxuICAgICAgICAgICAgICAgIHNwLnNwcml0ZUZyYW1lID0gc3ByaXRlRnJhbWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XHJcbn1cclxuXHJcbn1cclxuXHJcbn1cclxuIl19
