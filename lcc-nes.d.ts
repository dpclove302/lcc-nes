declare module lcc {
    class RingBuffer {
        private _elements;
        private _first;
        private _last;
        private _size;
        private _evictedCb;
        constructor(capacity: number, evictedCb?: Function);
        capacity(): number;
        size(): number;
        isEmpty(): boolean;
        isFull(): boolean;
        peek(): any;
        peekN(count: number): any[];
        deq(): any;
        deqN(count: number): any[];
        enq(element: any): number;
    }
}
declare module lcc {
    module Utils {
        function ab2bs(ab: ArrayBuffer): Promise<string>;
    }
}
declare module lcc {
    module nes {
        class AudioPlayer {
            private _buffer;
            private _audioCtx;
            private _scriptNode;
            constructor();
            getSampleRate(): number;
            start(): void;
            stop(): Promise<void>;
            writeSample(left: any, right: any): void;
            onaudioprocess(e: any): void;
        }
    }
}
declare module lcc {
    module nes {
        class Emulator extends cc.Component {
            _rom: cc.BufferAsset;
            get rom(): cc.BufferAsset;
            set rom(value: cc.BufferAsset);
            private _prepare;
            private _nes;
            private _texture;
            private _framebuff;
            private _frameu8;
            private _frameu32;
            private _audio;
            onLoad(): void;
            private setFrameBuffer;
            getNES(): any;
            getTexture(): cc.Texture2D;
            setRomData(data: cc.BufferAsset): Promise<void>;
            update(dt: any): void;
            start(): void;
            saveArchive(): any;
            loadArchive(json: any): void;
        }
    }
}
declare module lcc {
    module nes {
        enum Player {
            PLAYER_1 = 1,
            PLAYER_2 = 2
        }
        enum Button {
            A = 0,
            B = 1,
            SELECT = 2,
            START = 3,
            UP = 4,
            DOWN = 5,
            LEFT = 6,
            RIGHT = 7
        }
        class ButtonMap {
            button: Button;
            key: cc.macro.KEY;
            constructor(...args: any[]);
        }
        class Controller extends cc.Component {
            player: Player;
            _buttons: ButtonMap[];
            get buttons(): ButtonMap[];
            set buttons(value: ButtonMap[]);
            private _nes;
            private _keymap;
            onLoad(): void;
            onDestroy(): void;
            private onNodeButtonEvent;
            onButtonEvent(button: Button, down: boolean): void;
            private onKeyDown;
            private onKeyUp;
            private updateButtonMap;
        }
    }
}
declare module lcc {
    module nes {
        class DisplaySprites extends cc.Component {
            _sprites: cc.Sprite[];
            get sprites(): cc.Sprite[];
            set sprites(value: cc.Sprite[]);
            private _emulator;
            onLoad(): void;
            private setDisplaySprites;
        }
    }
}
